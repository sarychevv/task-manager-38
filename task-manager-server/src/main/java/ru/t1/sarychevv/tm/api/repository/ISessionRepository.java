package ru.t1.sarychevv.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    void update(@NotNull Session session) throws Exception;
}
