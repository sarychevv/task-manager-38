package ru.t1.sarychevv.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sarychevv.tm.api.service.IAuthService;
import ru.t1.sarychevv.tm.api.service.IServiceLocator;
import ru.t1.sarychevv.tm.dto.request.user.UserLoginRequest;
import ru.t1.sarychevv.tm.dto.request.user.UserLogoutRequest;
import ru.t1.sarychevv.tm.dto.request.user.UserProfileRequest;
import ru.t1.sarychevv.tm.dto.response.user.UserLoginResponse;
import ru.t1.sarychevv.tm.dto.response.user.UserLogoutResponse;
import ru.t1.sarychevv.tm.dto.response.user.UserProfileResponse;
import ru.t1.sarychevv.tm.model.Session;
import ru.t1.sarychevv.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.sarychevv.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public UserLoginResponse login(@WebParam(name = REQUEST, partName = REQUEST)
                                   @NotNull final UserLoginRequest request) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull String token = null;
        try {
            token = authService.login(request.getLogin(), request.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new UserLoginResponse(token);
    }

    @Override
    @NotNull
    @WebMethod
    public UserLogoutResponse logout(@WebParam(name = REQUEST, partName = REQUEST)
                                     @NotNull final UserLogoutRequest request) {
        final Session session = check(request);
        try {
            getServiceLocator().getAuthService().invalidate(session);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(@WebParam(name = REQUEST, partName = REQUEST)
                                       @NotNull final UserProfileRequest request) {
        @Nullable Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable User user = new User();
        try {
            user = getServiceLocator().getUserService().findOneById(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new UserProfileResponse(user);
    }

}

