package ru.t1.sarychevv.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.DBConstants;
import ru.t1.sarychevv.tm.api.repository.IRepository;
import ru.t1.sarychevv.tm.comparator.CreatedComparator;
import ru.t1.sarychevv.tm.comparator.StatusComparator;
import ru.t1.sarychevv.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> records = new ArrayList<>();

    @NotNull
    protected final Connection connection;

    protected AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    protected abstract String getTableName();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return DBConstants.COLUMN_CREATED;
        if (comparator == StatusComparator.INSTANCE) return DBConstants.COLUMN_STATUS;
        else return DBConstants.COLUMN_NAME;
    }

    @NotNull
    public abstract M fetch(@NotNull final ResultSet row) throws Exception;

    @NotNull
    public abstract M add(@NotNull final M model) throws Exception;

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) throws Exception {
        @NotNull List<M> result = new ArrayList<>();
        for (M model : models) {
            result.add(add(model));
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull String sql;
        if (comparator == null) sql = String.format("SELECT * FROM %s", getTableName());
        else sql = String.format("SELECT * FROM %s ORDER BY %s", getTableName(), getSortType(comparator));
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) throws Exception {
        removeAll();
        return add(models);
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s LIMIT ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            for (int i = 0; i < index - 1; i++) {
                resultSet.next();
            }
            return fetch(resultSet);
        }
    }

    @NotNull
    @Override
    public M removeOne(@NotNull final M model) throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String id) throws Exception {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final Integer index) throws Exception {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    public void removeAll(@NotNull final List<M> models) throws Exception {
        for (M model : models) {
            removeOne(model);
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    @NotNull
    @Override
    public Integer getSize() throws Exception {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @NotNull
    @Override
    public Boolean existsById(@NotNull final String id) throws Exception {
        return findOneById(id) != null;
    }

}
