package ru.t1.sarychevv.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.IProjectService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.enumerated.ProjectSort;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.exception.entity.ModelNotFoundException;
import ru.t1.sarychevv.tm.exception.field.*;
import ru.t1.sarychevv.tm.marker.DBCategory;
import ru.t1.sarychevv.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(DBCategory.class)
public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String TEST_NAME = "Test project";

    private static final String TEST_DESCRIPTION = "Test description";

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @Before
    public void initRepository() throws Exception {
        projectList = new ArrayList<>();
        projectService = new ProjectService(connectionService);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName(TEST_NAME + i);
            project.setDescription(TEST_DESCRIPTION + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectService.add(project);
            projectList.add(project);
        }
    }

    @After
    public void removeRepository() throws Exception {
        projectService.removeAll();
    }

    @Test
    public void testCreate() throws Exception {
        @Nullable final Project project = projectService.create(USER_ID_1, TEST_NAME);
        if (project == null) return;
        Assert.assertEquals(project, projectService.findOneById(project.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, projectService.getSize().intValue());
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithoutName() throws Exception {
        projectService.create(USER_ID_1, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithoutUserId() throws Exception {
        projectService.create(null, TEST_NAME, TEST_DESCRIPTION);
    }

    @Test
    public void testCreateWithDescription() throws Exception {
        @Nullable final Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        Assert.assertEquals(project, projectService.findOneById(project.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, projectService.getSize().intValue());
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithDescriptionWithoutName() throws Exception {
        projectService.create(USER_ID_1, null, TEST_DESCRIPTION);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithDescriptionWithoutUserId() throws Exception {
        projectService.create(null, TEST_NAME, TEST_DESCRIPTION);
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateWithoutDescription() throws Exception {
        projectService.create(USER_ID_1, TEST_NAME, null);
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final Project project = new Project();
        projectService.add(project);
        Assert.assertEquals(true, projectService.existsById(project.getId()));
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddWithoutProject() throws Exception {
        @Nullable final Project project = null;
        projectService.add(project);
    }

    @Test
    public void testAddForUser() throws Exception {
        @NotNull final Project project = new Project();
        project.setUserId(USER_ID_1);
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectService.add(project);
        Assert.assertEquals(true, projectService.existsById(project.getId()));
    }

    @Test
    public void testExistsById() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        Assert.assertEquals(true, projectService.existsById(project.getId()));
    }

    @Test(expected = IdEmptyException.class)
    public void testExistsByIdWithoutId() throws Exception {
        projectService.existsById(null);
    }

    @Test
    public void testExistsByIdForUser() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        Assert.assertEquals(true, projectService.existsById(USER_ID_1, project.getId()));
    }

    @Test(expected = IdEmptyException.class)
    public void testExistsByIdForUserWithoutId() throws Exception {
        projectService.existsById(USER_ID_1, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExistsByIdForUserWithoutUserId() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.existsById(null, project.getId());
    }

    @Test
    public void testUpdateById() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое наименование";
        projectService.updateById(USER_ID_1, project.getId(), testName, testDescription);
        Assert.assertEquals(testName, projectService.findOneById(project.getId()).getName());
        Assert.assertEquals(testDescription, projectService.findOneById(project.getId()).getDescription());
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByIdWithoutId() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateById(USER_ID_1, null, testName, testDescription);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdWithoutName() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateById(USER_ID_1, project.getId(), null, testDescription);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdWithoutUserId() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateById(null, project.getId(), testName, testDescription);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testUpdateByIdWithWrongId() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateById(USER_ID_1, "501", testName, testDescription);
    }

    @Test
    public void testUpdateByIndex() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое наименование";
        projectService.updateByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1, testName, testDescription);
        Assert.assertEquals(testName, projectService.findOneById(project.getId()).getName());
        Assert.assertEquals(testDescription, projectService.findOneById(project.getId()).getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexWithoutIndex() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateByIndex(USER_ID_1, null, testName, testDescription);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexWithoutName() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1, null, testDescription);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndexWithoutUserId() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        projectService.updateByIndex(null, NUMBER_OF_ENTRIES / 2 + 1, testName, testDescription);
    }

    @Test
    public void testChangeStatusById() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusById(USER_ID_1, project.getId(), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findOneById(project.getId()).getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeStatusByIdWithoutId() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusById(USER_ID_1, null, Status.COMPLETED);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeStatusByIdWithoutUserId() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusById(null, project.getId(), Status.COMPLETED);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeStatusByIdWithoutStatus() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusById(USER_ID_1, project.getId(), null);
    }

    @Test
    public void testChangeStatusByIndex() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findOneById(project.getId()).getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusByIdWithoutIndex() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusByIndex(USER_ID_1, null, Status.COMPLETED);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeStatusByIdWithoutUserIndex() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusByIndex(null, NUMBER_OF_ENTRIES / 2 + 1, Status.COMPLETED);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeStatusByIndexWithoutStatus() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.changeStatusByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1, null);
    }

    @Test
    public void testFindAll() throws Exception {
        Assert.assertEquals(projectService.getSize().intValue(), projectService.findAll().size());
    }

    @Test
    public void testFindAllForUser() throws Exception {
        Assert.assertEquals(projectService.getSize().intValue() / 2 + 1, projectService.findAll(USER_ID_1).size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForUserWithoutUserId() throws Exception {
        @Nullable String testUserId = null;
        projectService.findAll(testUserId);
    }

    @Test
    public void testFindAllForUserWithComparator() throws Exception {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        Assert.assertEquals(projectService.getSize().intValue() / 2 + 1, projectService.findAll(USER_ID_1, sort.getComparator()).size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForUserWithComparatorWithoutUserId() throws Exception {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        projectService.findAll(null, sort.getComparator());
    }

    @Test
    public void testFindAllForUserWithComparatorWithoutComparator() throws Exception {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        Assert.assertEquals(projectService.getSize().intValue() / 2 + 1, projectService.findAll(USER_ID_1, null).size());
    }

    @Test
    public void testFindOneById() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        Assert.assertEquals(TEST_NAME, projectService.findOneById(project.getId()).getName());
        Assert.assertEquals(TEST_DESCRIPTION, projectService.findOneById(project.getId()).getDescription());
    }

    @Test
    public void testFindOneByIdForUser() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        Assert.assertEquals(TEST_NAME, projectService.findOneById(USER_ID_1, project.getId()).getName());
        Assert.assertEquals(TEST_DESCRIPTION, projectService.findOneById(USER_ID_1, project.getId()).getDescription());
    }


    @Test(expected = UserIdEmptyException.class)
    public void testFindOneByIdForUserWithoutUserId() throws Exception {
        @Nullable Project project = projectService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (project == null) return;
        projectService.findOneById(null, project.getId());
    }

    @Test
    public void testGetSize() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize().intValue());
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectService.getSize(USER_ID_1));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSizeForUserWithoutUserId() throws Exception {
        projectService.getSize(null);
    }
}
